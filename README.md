```bash
git clone --bare git@bitbucket.org:wallace11/portable-dotfiles.git ~/.dotfiles
alias dots="git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
dots checkout
# Optional
dots remote add write https://wallace11@bitbucket.org/wallace11/portable-dotfiles.git
```